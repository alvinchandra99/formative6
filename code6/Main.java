import java.io.FileWriter;   
import java.io.IOException;

class JakartaPlat extends Thread{
    private String hurufDepan = "B";
    private String hurufBelakang;
    private String angka;
    private String plat;
    final char[] alphabet = {'A', 'B','C','D','E','F','G','H','I','J','K','L','M','N',
                            'O','P','Q','R','S','T','U','V','W','X','Y','Z'};

    @Override
    public void run(){
        try{
            FileWriter platJakarta = new FileWriter("jakarta.txt");
            for(int i=1 ; i <=9999; i++){
                int length = String.valueOf(i).length();
                
                if(length == 1){
                    angka = "000"+i;
                }
                if(length ==2){
                    angka = "00"+i;
                }
                if(length ==3){
                    angka = "0"+i;
                }
                if(length ==4){
                    angka = String.valueOf(i);
                }
    
                for(char j: alphabet){
                    for(char k:alphabet){
                        hurufBelakang = new StringBuilder().append(j).append(k).toString();
                        plat = (hurufDepan +" " +angka+" " + hurufBelakang +"\n");
                        platJakarta.write(plat);
                    }
                }
            }

            platJakarta.close();


        }
        catch (IOException e) {
            System.out.println("An error occurred.");
        }
    }
}

class BogorPlat extends Thread{
    private String hurufDepan = "F";
    private String hurufBelakang;
    private String angka;
    private String plat;
    final char[] alphabet = {'A', 'B','C','D','E','F','G','H','I','J','K','L','M','N',
                            'O','P','Q','R','S','T','U','V','W','X','Y','Z'};
    
    @Override
    public void run(){
        try{
            FileWriter platBogor = new FileWriter("bogor.txt");
            for(int i=1 ; i <=9999; i++){
                int length = String.valueOf(i).length();
                
                if(length == 1){
                    angka = "000"+i;
                }
                if(length ==2){
                    angka = "00"+i;
                }
                if(length ==3){
                    angka = "0"+i;
                }
                if(length ==4){
                    angka = String.valueOf(i);
                }
    
                for(char j: alphabet){
                    for(char k:alphabet){
                        hurufBelakang = new StringBuilder().append(j).append(k).toString();
                        plat = (hurufDepan +" " +angka+" " + hurufBelakang +"\n");
                        platBogor.write(plat);
                    }
                }
            }

            platBogor.close();


        }
        catch (IOException e) {
            System.out.println("An error occurred.");
        }
    }
}




class Main{
    public static void main(String[] args) {
        JakartaPlat jakarta = new JakartaPlat();
        BogorPlat bogor = new BogorPlat();

        jakarta.start();
        bogor.start();

        try { 
            jakarta.join();
            bogor.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
    }
}